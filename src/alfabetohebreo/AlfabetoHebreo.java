/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alfabetohebreo;

import java.awt.Toolkit;

/**
 *
 * @author Josue Daniel Roldan Ochoa.
 */
public class AlfabetoHebreo extends javax.swing.JFrame {

    /**
     * Creates new form AlfabetoHebreo
     */
    public AlfabetoHebreo() {
        initComponents();
   setIconImage(Toolkit.getDefaultToolkit().getImage(this.getClass().getResource("/alfabetohebreo/utiles.png")));
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("ALEF-BET");
        setAlwaysOnTop(true);
        setResizable(false);

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {"1", "ALEF", "א"},
                {"2", "BET", "ב"},
                {"3", "GUIMEL", "ג"},
                {"4", "DALET", "ד"},
                {"5", "HAI", "ה"},
                {"6", "VAV", "ו"},
                {"7", "ZAYIN", "ז"},
                {"8", "HET", "ח"},
                {"9", "TET", "ט"},
                {"10", "YOD", "י"},
                {"11", "KAF", "כ"},
                {"11", "JAF SOFIT", "ך"},
                {"12", "LAMED", "ל"},
                {"13", "MEM", "מ"},
                {"13", "MEM SOFIT", "ם"},
                {"14", "NUN", "נ"},
                {"14", "NUN SOFIT", "ן"},
                {"15", "SAMEJ", "ס"},
                {"16", "AYIN", "ע"},
                {"17", "PE", "פ"},
                {"17", "FE SOFIT", "ף"},
                {"18", "TZADI", "צ"},
                {"18", "TZADI SOFIT", "ץ"},
                {"19", "KUF", "ק"},
                {"20", "REISH", "ר"},
                {"21", "SHIN", "ש"},
                {"22", "TAV", "ת"}
            },
            new String [] {
                "NUMERO", "NOMBRE", "LETRA"
            }
        ));
        jScrollPane1.setViewportView(jTable1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 515, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 454, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(AlfabetoHebreo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(AlfabetoHebreo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(AlfabetoHebreo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(AlfabetoHebreo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new AlfabetoHebreo().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    // End of variables declaration//GEN-END:variables
}
